package com.muvastudios.magiceggfarm;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;

public class Main {
	private static boolean rebuildAtlas = false;
	private static boolean drawDebugOutline = false;

	public static void main(String[] args) {
		if (rebuildAtlas) {
			Settings settings = new Settings();
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			settings.debug = drawDebugOutline;
			TexturePacker2.process(settings, "assets-raw/images",
					"../MagicEggFarm-android/assets/images",
					"magiceggfarm.pack");
			TexturePacker2.process(settings, "assets-raw/images-ui",
					"../MagicEggFarm-android/assets/images",
					"magiceggfarm-ui.pack");
		}
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Magic Egg Farm " + MagicEggFarmMain.VERSION;
		cfg.useGL20 = true;
		cfg.width = 800;
		cfg.height = 480;

		new LwjglApplication(new MagicEggFarmMain(), cfg);
	}
}
