package com.muvastudios.magiceggfarm.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Disposable;
import com.muvastudios.magiceggfarm.game.objects.Bird.EggUnit;
import com.muvastudios.magiceggfarm.game.objects.Ground.GroundUnit;
import com.muvastudios.magiceggfarm.util.Constants;
import com.muvastudios.magiceggfarm.util.GamePreferences;

public class WorldRenderer implements Disposable {

	// TAG for debugging
	private static final String TAG = WorldRenderer.class.getName();

	// Initialize WorldController
	private WorldController worldController;
	private SpriteBatch batch;
	private OrthographicCamera camera;
	private OrthographicCamera cameraGUI;
	private ShapeRenderer sr;
	private boolean dev_mode = false;

	public WorldRenderer(WorldController worldController) {
		this.worldController = worldController;
		init();
	}

	private void init() {
		Gdx.app.debug(TAG, "Setting up World objects");
		batch = new SpriteBatch();

		// The World Camera
		camera = new OrthographicCamera(Constants.VIEWPORT_WIDTH,
				Constants.VIEWPORT_HEIGHT);
		camera.position.set(0, 0, 0);
		camera.update();

		// The GUI Camera
		cameraGUI = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH,
				Constants.VIEWPORT_GUI_HEIGHT);
		cameraGUI.position.set(0, 0, 0);
		// Set the camera to flipY
		cameraGUI.setToOrtho(true);
		cameraGUI.update();

		sr = new ShapeRenderer();
	}

	public void render() {
		renderWorld(batch);
		renderGUI(batch);
	}

	private void renderWorld(SpriteBatch batch) {
		worldController.cameraHelper.applyTo(camera);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		worldController.level.render(batch);
		batch.end();
		drawSr();
	}

	private void renderGUI(SpriteBatch batch) {
		batch.setProjectionMatrix(cameraGUI.combined);
		batch.begin();

		// Draw the score at the top-center part of the screen
		renderGUIScore(batch);

		// Draw the FPS
		if (GamePreferences.instance.showFpsCounter)
			renderFpsCounter(batch);

		// Draw the Timer
		renderGUITimer(batch);

		// Draw the Target
		renderGUITarget(batch);

		// Draw the Game Over Message
		renderGUIGameOverMessage();
		batch.end();
	}

	private void renderGUIScore(SpriteBatch batch) {
		float x = cameraGUI.viewportWidth / 2;
		float y = 20;
		BitmapFont scoreFont = Assets.instance.fonts.defaultNormal;
		scoreFont.draw(batch, "" + (int)worldController.scoreVisual, x, y);
	}

	private void renderGUITimer(SpriteBatch batch) {
		float x = cameraGUI.viewportWidth - 100;
		float y = 20;
		TextureRegion reg = Assets.instance.timer.timer;
		BitmapFont timerFont = Assets.instance.fonts.defaultSmall;
		float timer = worldController.timeLeft;
		if (timer >= 40) {
			// FPS 45 or More show up in GREEN
			timerFont.setColor(1, 1, 1, 1);
			batch.setColor(1, 1, 1, 1);
		} else if (timer >= 20) {
			// FPS 30 ore more show up in YELLOW
			timerFont.setColor(1, 1, 0, 1);
			batch.setColor(1, 1, 0, 1);
		} else {
			// FPS less than 30 show up in RED
			timerFont.setColor(1, 0, 0, 1);
			batch.setColor(1, 0, 0, 1);
		}
		batch.draw(reg.getTexture(), x, y - 10, 0, 0, 50, 50, 1, 1, 0,
				reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(),
				reg.getRegionHeight(), false, false);
		Assets.instance.fonts.defaultSmall.draw(batch, "" + (int) timer,
				x + 50, y);
		timerFont.setColor(1, 1, 1, 1);
		batch.setColor(1, 1, 1, 1);
	}

	private void renderGUITarget(SpriteBatch batch) {
		float x = 20;
		float y = 10;

		Assets.instance.fonts.defaultSmall.draw(batch, "Target:" + " "
				+ worldController.target, x, y);
	}

	private void renderFpsCounter(SpriteBatch batch) {
		float x = cameraGUI.viewportWidth - 120;
		float y = cameraGUI.viewportHeight - 70;
		int fps = Gdx.graphics.getFramesPerSecond();

		BitmapFont fpsFont = Assets.instance.fonts.defaultSmall;
		if (fps >= 45) {
			// FPS 45 or More show up in GREEN
			fpsFont.setColor(0, 1, 0, 1);
		} else if (fps >= 30) {
			// FPS 30 ore more show up in YELLOW
			fpsFont.setColor(1, 1, 0, 1);
		} else {
			// FPS less than 30 show up in RED
			fpsFont.setColor(1, 0, 0, 1);
		}
		fpsFont.draw(batch, "fps: " + fps, x, y);
		fpsFont.setColor(1, 1, 1, 1);

	}

	public void renderGUIGameOverMessage() {
		float x = cameraGUI.viewportWidth / 2;
		float y = cameraGUI.viewportHeight / 2;
		if (worldController.isGameOver()) {
			BitmapFont fontGameOver = Assets.instance.fonts.defaultBig;
			fontGameOver.setColor(1, 0.75f, 0.25f, 1);
			fontGameOver.drawMultiLine(batch, "GAME OVER", x, y, 0,
					BitmapFont.HAlignment.CENTER);
			fontGameOver.setColor(1, 1, 1, 1);
		}
	}

	public void resize(int width, int height) {

		// Resize World Camera
		camera.viewportWidth = (Constants.VIEWPORT_HEIGHT / (float) height)
				* (float) width;
		camera.update();

		// Resize GUI Camera
		cameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
		cameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_WIDTH / (float) height)
				* width;
		cameraGUI.position.set(cameraGUI.viewportWidth / 2,
				cameraGUI.viewportHeight / 2, 0);
		cameraGUI.update();

	}

	@Override
	public void dispose() {
		batch.dispose();
	}

	private void drawSr() {
		if (!dev_mode)
			return;
		sr.setProjectionMatrix(camera.combined);
		sr.begin(ShapeType.Line);
		sr.setColor(Color.RED);
		sr.rect(worldController.level.bucket.position.x,
				worldController.level.bucket.position.y,
				worldController.level.bucket.bounds.width,
				worldController.level.bucket.bounds.height);
		sr.setColor(Color.ORANGE);
		for (GroundUnit unit : worldController.level.ground.ground)
			sr.rect(unit.position.x - 5f, unit.position.y - 2.52f,
					unit.bounds.width, unit.bounds.height);
		sr.setColor(Color.MAGENTA);
		for (EggUnit unit : worldController.level.bird.normalEggArray) {
			sr.rect(unit.position.x - 2.5f, unit.position.y + 0.3f,
					unit.bounds.width, unit.bounds.height);
		}
		for (EggUnit unit : worldController.level.bird.badEggArray) {
			sr.rect(unit.position.x - 2.5f, unit.position.y + 0.3f,
					unit.bounds.width, unit.bounds.height);
		}
		for (EggUnit unit : worldController.level.bird.bonusEggArray) {
			sr.rect(unit.position.x - 2.5f, unit.position.y + 0.3f,
					unit.bounds.width, unit.bounds.height);
		}
		sr.end();
	}

}
