package com.muvastudios.magiceggfarm.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.muvastudios.magiceggfarm.game.objects.Background;
import com.muvastudios.magiceggfarm.game.objects.Bird;
import com.muvastudios.magiceggfarm.game.objects.Bucket;
import com.muvastudios.magiceggfarm.game.objects.Clouds;
import com.muvastudios.magiceggfarm.game.objects.Grass;
import com.muvastudios.magiceggfarm.game.objects.Grasslands;
import com.muvastudios.magiceggfarm.game.objects.Ground;

public class Level {
	public static final String TAG = Level.class.getName();
	// level objects
	public Ground ground;
	public Bird bird;
	public Bucket bucket;

	// decoration
	public Clouds clouds;
	public Grasslands grasslands;
	public Background background;
	public Grass grass;

	public Level() {
		init();
	}

	private void init() {
		bucket = new Bucket();
		ground = new Ground();
		bird = new Bird(10, 3);

		// Decoration
		background = new Background();
		grasslands = new Grasslands(10);
		grasslands.position.set(1, -1);
		clouds = new Clouds(10);
		clouds.position.set(0, 2);
		grass = new Grass(10);
	}

	public void update(float delta) {
		bucket.update(delta);
		ground.update(delta);
		clouds.update(delta);
		grass.update(delta);
		bird.update(delta);
	}

	public void render(SpriteBatch batch) {

		// draw background
		background.render(batch);

		// draw grasslands
		grasslands.render(batch);

		// draw clouds
		clouds.render(batch);

		// draw bird
		bird.render(batch);

		// draw ground
		ground.render(batch);

		// draw bucket
		bucket.render(batch);

		// draw grass
		grass.render(batch);

	}

}
