package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.muvastudios.magiceggfarm.game.Assets;

public class Grasslands extends AbstractGameObject {
	// Containers for both grasslands
	private Array<TextureRegion> regGrassland1;
	private Array<TextureRegion> regGrassland2;

	// Containers for individual grass units
	private Array<Land1> land1Array;
	private Array<Land2> land2Array;

	// Length
	private float length;

	/**
	 * Class for GrasslandOne
	 * 
	 * @author sam
	 * 
	 */
	private class Land1 extends AbstractGameObject {
		private TextureRegion regLand1;

		public Land1() {
		}

		public void setRegion(TextureRegion region) {
			regLand1 = region;
		}

		@Override
		public void render(SpriteBatch batch) {
			TextureRegion reg = regLand1;
			float relX = -5.015f;
			float relY = -2.35f;
			batch.draw(reg.getTexture(), position.x + relX, position.y + relY,
					origin.x, origin.y, dimension.x, dimension.y, scale.x,
					scale.y, rotation, reg.getRegionX(), reg.getRegionY(),
					reg.getRegionWidth(), reg.getRegionHeight(), false, false);
		}

	}

	/**
	 * Class for GrasslandTwo
	 * 
	 * @author Sam
	 * 
	 */

	private class Land2 extends AbstractGameObject {
		private TextureRegion regLand2;

		public Land2() {
		}

		public void setRegion(TextureRegion region) {
			regLand2 = region;
		}

		@Override
		public void render(SpriteBatch batch) {
			TextureRegion reg = regLand2;
			float relX = -5.015f;
			float relY = -2.35f;

			batch.draw(reg.getTexture(), position.x + relX, position.y + relY,
					origin.x, origin.y, dimension.x, dimension.y, scale.x,
					scale.y, rotation, reg.getRegionX(), reg.getRegionY(),
					reg.getRegionWidth(), reg.getRegionHeight(), false, false);
		}

	}

	public Grasslands(float length) {
		this.length = length;
		init();

	}

	private void init() {
		// Set dimension
		dimension.set(5.01f, 2.4f);

		// Initialize Holder array for Grassland 2 and add texture
		regGrassland2 = new Array<TextureRegion>();
		regGrassland2.add(Assets.instance.levelDecoration.grassland02);

		// Initialize Holder array for Grassland 1 and add Texture
		regGrassland1 = new Array<TextureRegion>();
		regGrassland1.add(Assets.instance.levelDecoration.grassland01);

		// Distfactor
		int distFactor = 5;
		int numLand = (int) (length / distFactor);

		// initialize land1 array
		land2Array = new Array<Grasslands.Land2>(numLand);
		land1Array = new Array<Grasslands.Land1>(numLand);
		for (int i = 0; i < numLand; i++) {

			// land 2
			Land2 layer2 = setLand2();
			layer2.position.x = (i * distFactor);
			land2Array.add(layer2);

			// land 1
			Land1 layer1 = setLand1();
			layer1.position.x = (i * distFactor);
			land1Array.add(layer1);
		}

	}

	private Land2 setLand2() {
		Land2 layer2 = new Land2();
		layer2.dimension.set(5.035f, 1.5f);

		// set the right region
		layer2.setRegion(regGrassland2.get(0));

		return layer2;
	}

	private Land1 setLand1() {
		Land1 layer1 = new Land1();
		layer1.dimension.set(dimension);

		// set the right region
		layer1.setRegion(regGrassland1.get(0));

		return layer1;
	}

	@Override
	public void render(SpriteBatch batch) {
		for (Land2 layer2 : land2Array)
			layer2.render(batch);
		for (Land1 layer1 : land1Array)
			layer1.render(batch);

	}

}
