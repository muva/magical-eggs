package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.muvastudios.magiceggfarm.game.Assets;

public class Ground extends AbstractGameObject {
	private Array<TextureRegion> regGround;
	public Array<GroundUnit> ground;

	public Ground() {
		init();
	}

	public class GroundUnit extends AbstractGameObject {
		private TextureRegion groundReg;

		public GroundUnit() {
		}

		public void setRegion(TextureRegion region) {
			groundReg = region;
		}

		@Override
		public void render(SpriteBatch batch) {
			TextureRegion reg = groundReg;
			float relX = -5;
			float relY = -2.52f;
			bounds.set(position.x + relX, position.y + relY, dimension.x, dimension.y);
			batch.draw(reg.getTexture(), position.x + relX, position.y + relY,
					origin.x, origin.y, dimension.x, dimension.y, scale.x,
					scale.y, 0, reg.getRegionX(), reg.getRegionY(),
					reg.getRegionWidth(), reg.getRegionHeight(), false, false);
		}
	}

	private void init() {
		ground = new Array<Ground.GroundUnit>();
		regGround = new Array<TextureRegion>();
		regGround.add(Assets.instance.ground.ground);
		dimension.set(2.0f, 0.2f);
		int groundNum = 5;
		int distFac = 2;
		for (int i = 0; i < groundNum; i++) {
			GroundUnit groundUnit = spawnGround();
			groundUnit.position.x = i * distFac;
			ground.add(groundUnit);

		}
	}

	private GroundUnit spawnGround() {
		GroundUnit groundUnit = new GroundUnit();
		groundUnit.dimension.set(dimension);
		groundUnit.setRegion(regGround.first());
		return groundUnit;
	}

	@Override
	public void render(SpriteBatch batch) {
		for (GroundUnit unit : ground)
			unit.render(batch);
	}

}
