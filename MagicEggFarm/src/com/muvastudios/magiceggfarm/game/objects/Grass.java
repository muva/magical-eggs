package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.muvastudios.magiceggfarm.game.Assets;

public class Grass extends AbstractGameObject {
	// Container Array for All Grass Units
	private Array<TextureRegion> regGrass;

	// Container for just one grass unit
	private Array<GrassUnit> grass;

	// Length that they will cover
	private float length;

	

	// Constructor for Grass class
	public Grass(float length) {
		this.length = length;
		init();
	}

	private void init() {

		// Set the dimensions for each grass unit
		dimension.set(2.0f, 0.18f);
		setAnimation(Assets.instance.levelDecoration.grassAnimation);
		// Initialize the array and add the grass unit
		regGrass = new Array<TextureRegion>();
		regGrass.add(animation.getKeyFrame(stateTime, true));
		// Distance Factor to help get the overal number of units that should be
		// drawn in accordance to the set length
		int distFactor = 2;

		// Number of grass units that should be drawn
		int numGrassUnits = (int) (length / distFactor);
		grass = new Array<Grass.GrassUnit>(numGrassUnits);

		// Add the appropriate grass units to the array and set positions
		for (int i = 0; i < numGrassUnits; i++) {
			GrassUnit grassUnit = spawnGrass();
			grassUnit.position.x = (i * distFactor);
			grass.add(grassUnit);
		}
	}

	// Set dimension, the region to be drawn and spawn the number of grass units
	private GrassUnit spawnGrass() {
		GrassUnit grassUnit = new GrassUnit();
		grassUnit.dimension.set(dimension);

		return grassUnit;
	}

	@Override
	public void render(SpriteBatch batch) {
		for (GrassUnit grassUnit : grass)
			grassUnit.render(batch);

	}
	
	@Override 
	public void update(float delta){
		for (GrassUnit grassUnit : grass){
			grassUnit.update(delta);
		}
	}
	
	// Private class to draw each grass unit
		private class GrassUnit extends AbstractGameObject {

			public GrassUnit() {
				setAnimation(Assets.instance.levelDecoration.grassAnimation);
				stateTime = MathUtils.random(0.0f, 1.0f);
			}

			@Override
			public void render(SpriteBatch batch) {
				TextureRegion reg = animation.getKeyFrame(stateTime, true);
				float relX = -5;
				float relY = -2.35f;
				batch.draw(reg.getTexture(), position.x + origin.x + relX,
						position.y + origin.y + relY, origin.x, origin.y,
						dimension.x, dimension.y, scale.y, scale.y, rotation,
						reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(),
						reg.getRegionHeight(), false, false);
			}
			
			@Override 
			public void update(float delta){
				super.update(delta);
			}
		}

}
