package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.muvastudios.magiceggfarm.game.Assets;

public class Bird extends AbstractGameObject {
	private Array<TextureRegion> birds;
	private Array<BirdUnit> birdUnit;
	private Array<TextureRegion> eggs;
	public Array<EggUnit> normalEggArray;
	public Array<EggUnit> badEggArray;
	public Array<EggUnit> bonusEggArray;
	public EggUnit eUnit = new EggUnit();

	private float length;
	private int numBirds;
	private Vector2 posy;
	private float eggSpawnTime;
	private float badEggSpawnTime;
	private float bonusEggSpawnTime;

	public Bird(float length, int numBirds) {
		this.length = length;
		this.numBirds = numBirds;
		init();
	}

	private void init() {
		dimension.set(0.9f, 0.8f);
		setAnimation(Assets.instance.bird.birdsAnimation);
		birds = new Array<TextureRegion>();
		for (int i = 0; i < numBirds; i++)
			birds.add(animation.getKeyFrame(stateTime, true));
		birdUnit = new Array<Bird.BirdUnit>();
		int distFact = numBirds;
		int birdNum = (int) (length / distFact);
		for (int i = 0; i < birdNum; i++) {
			BirdUnit unit = spawnBird();
			unit.position.x = i * distFact;
			birdUnit.add(unit);
		}

		eggs = new Array<TextureRegion>();
		eggs.add(Assets.instance.egg.egg);
		eggs.add(Assets.instance.egg.badEgg);
		eggs.add(Assets.instance.egg.bonusEgg);
		normalEggArray = new Array<EggUnit>();
		badEggArray = new Array<EggUnit>();
		bonusEggArray = new Array<EggUnit>();
	}

	@Override
	public void render(SpriteBatch batch) {
		for (BirdUnit unit : birdUnit)
			unit.render(batch);
		for (EggUnit unitEggs : normalEggArray)
			unitEggs.render(batch);

		for (EggUnit unitEggs : badEggArray)
			unitEggs.render(batch);

		for (EggUnit unitEggs : bonusEggArray)
			unitEggs.render(batch);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		for (BirdUnit unit : birdUnit)
			unit.update(delta);

		generateEggs();
		for (EggUnit unitEgg : normalEggArray) {
			unitEgg.update(delta);
			if (unitEgg.position.y < -2.65f)
				normalEggArray.removeValue(unitEgg, false);
		}

		for (EggUnit unitEgg : badEggArray) {
			unitEgg.update(delta);
			if (unitEgg.position.y < -2.65f)
				badEggArray.removeValue(unitEgg, false);
		}

		for (EggUnit unitEgg : bonusEggArray) {
			unitEgg.update(delta);
			if (unitEgg.position.y < -2.65f)
				bonusEggArray.removeValue(unitEgg, false);
		}
	}

	private BirdUnit spawnBird() {
		BirdUnit unit = new BirdUnit();
		unit.dimension.set(dimension);

		// position
		Vector2 pos = new Vector2();
		pos.x = 6.5f; // position after level
		pos.y += 0.8f; // base position

		// random additional position
		pos.y += MathUtils.random(0.0f, 0.2f)
				* (MathUtils.randomBoolean() ? 1 : -1);
		unit.position.set(pos);
		return unit;
	}

	private void generateEggs() {
		float badEggTime = 2500000000f;
		float bonusEggTime = 5500000000f;
		if (TimeUtils.nanoTime() - eggSpawnTime > 1000000000) {
			EggUnit unitE = spawnEgg();
			normalEggArray.add(unitE);
			Gdx.app.debug("Eggs", "" + normalEggArray.size);
		}
		if (TimeUtils.nanoTime() - badEggSpawnTime > badEggTime) {
			EggUnit unitE = spawnBadEgg();
			badEggArray.add(unitE);
			Gdx.app.debug("BadEggs", "" + badEggArray.size);
		}
		if (TimeUtils.nanoTime() - bonusEggSpawnTime > bonusEggTime) {
			EggUnit unitE = spawnBonusEgg();
			bonusEggArray.add(unitE);
		}
	}

	private EggUnit spawnEgg() {
		EggUnit eggUnit = new EggUnit();

		eggUnit.setRegion(eggs.get(0));
		Vector2 pos = new Vector2();
		pos.set(birdUnit.random().position.x, birdUnit.random().position.y);

		eggUnit.position.set(pos);
		eggSpawnTime = TimeUtils.nanoTime();
		return eggUnit;
	}

	private EggUnit spawnBadEgg() {
		EggUnit bUnit = new EggUnit();

		bUnit.setRegion(eggs.get(1));
		Vector2 pos = new Vector2();
		pos.set(birdUnit.random().position.x, birdUnit.random().position.y);

		bUnit.position.set(pos);
		badEggSpawnTime = TimeUtils.nanoTime();
		return bUnit;
	}

	private EggUnit spawnBonusEgg() {
		EggUnit bUnit = new EggUnit();

		bUnit.setRegion(eggs.get(2));
		Vector2 pos = new Vector2();
		pos.set(birdUnit.random().position.x, birdUnit.random().position.y);

		bUnit.position.set(pos);
		bonusEggSpawnTime = TimeUtils.nanoTime();
		return bUnit;
	}

	public int getScore() {
		return 15;
	}

	public int getBadEggScore() {
		return -20;
	}

	public int getBonusEggScore() {
		return 20;
	}

	/**
	 * A single bird unit
	 * 
	 * @author sam
	 * 
	 */
	public class BirdUnit extends AbstractGameObject {

		public BirdUnit() {
			posy = new Vector2();
			setAnimation(Assets.instance.bird.birdsAnimation);
			stateTime = MathUtils.random(1.0f, 5.0f);
		}

		@Override
		public void render(SpriteBatch batch) {
			float relX = -2.5f;
			float relY = 0.5f;
			TextureRegion reg = animation.getKeyFrame(stateTime, true);
			batch.draw(reg.getTexture(), position.x + relX, position.y + relY,
					origin.x, origin.y, dimension.x, dimension.y, scale.x,
					scale.y, rotation, reg.getRegionX(), reg.getRegionY(),
					reg.getRegionWidth(), reg.getRegionHeight(), false, false);
		}

		@Override
		public void update(float delta) {
			super.update(delta);
			terminalVelocity.set(1, 1);
			velocity.x = -terminalVelocity.x;
			if (position.x < -2.3f)
				position.x = 6.5f;
			posy.set(position.x, position.y);
		}

	}

	public class EggUnit extends AbstractGameObject {
		private TextureRegion eggReg;

		public EggUnit() {
			dimension.set(0.3f, 0.3f);
		}

		public void setRegion(TextureRegion region) {
			eggReg = region;
		}

		@Override
		public void render(SpriteBatch batch) {
			TextureRegion reg = eggReg;
			float relX = -2.5f;
			float relY = 0.3f;
			bounds.set(-2.5f, 0.3f, dimension.x, dimension.y);
			batch.draw(reg.getTexture(), position.x + relX, position.y + relY,
					origin.x, origin.y, dimension.x, dimension.y, scale.x,
					scale.y, rotation, reg.getRegionX(), reg.getRegionY(),
					reg.getRegionWidth(), reg.getRegionHeight(), false, false);
		}

		@Override
		public void update(float delta) {
			super.update(delta);
			terminalVelocity.set(0, 4.0f);
			velocity.y = -terminalVelocity.y;
		}

	}
}
