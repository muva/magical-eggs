package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.muvastudios.magiceggfarm.game.Assets;
import com.muvastudios.magiceggfarm.util.CharacterSkin;
import com.muvastudios.magiceggfarm.util.GamePreferences;

public class Bucket extends AbstractGameObject {
	public static final String TAG = Bucket.class.getName();
	private final float JUMP_TIME_MAX = 0.3f;
	private final float JUMP_TIME_MIN = 0.1f;

	public enum JUMP_STATE {
		GROUNDED, FALLING, JUMP_RISING, JUMP_FALLING
	}

	private TextureRegion regBucket;
	public float timeJumping;
	public JUMP_STATE jumpState;

	public Bucket() {
		init();
	}

	private void init() {
		dimension.set(1.25f, 0.6f);
		regBucket = Assets.instance.bucket.bucket;

		// Center Image on the game object
		origin.set(dimension.x / 2, dimension.y / 2);
		position.set(-0.5f, -2.0f);

		// Bounding box for collision detection
		bounds.set(position.x, position.y, dimension.x, dimension.y);

		// Set Physics Values
		terminalVelocity.set(6.0f, 4.0f);
		friction.set(12.0f, 0.0f);
		acceleration.set(0.0f, -25.0f);

		// Jump state
		jumpState = JUMP_STATE.FALLING;
		timeJumping = 0;
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		if (position.x <= -4.3f)
			position.x = -4.3f;
		else if (position.x >= 3f)
			position.x = 3f;
	}

	public void setJumping(boolean jumpKeyPressed) {
		switch (jumpState) {
		case GROUNDED: // Character is down the on the platform
			if (jumpKeyPressed) {
				// Start counting jump time from beginning
				timeJumping = 0;
				jumpState = JUMP_STATE.JUMP_RISING;
			}
			break;
		case JUMP_RISING: // Rising in the air
			if (!jumpKeyPressed)
				jumpState = JUMP_STATE.JUMP_FALLING;
			break;
		case FALLING: // Falling down
		case JUMP_FALLING: // Falling down after jump
			if (jumpKeyPressed) {
				jumpState = JUMP_STATE.JUMP_RISING;
			}
			break;
		}
	}

	@Override
	protected void updateMotionY(float delta) {
		switch (jumpState) {
		case GROUNDED:
			jumpState = JUMP_STATE.FALLING;
			break;
		case JUMP_RISING:
			// Keep track of jump time
			timeJumping += delta;
			// Jump time left?
			if (timeJumping <= JUMP_TIME_MAX) {
				// Still jumping
				velocity.y = terminalVelocity.y;
			}
			break;
		case FALLING:
			break;
		case JUMP_FALLING:
			// Add delta times to track jump time
			timeJumping += delta;
			// Jump to minimal height if jump key was pressed too short
			if (timeJumping > 0 && timeJumping <= JUMP_TIME_MIN) {
				// Still jumping
				velocity.y = terminalVelocity.y;
			}
		}
		if (jumpState != JUMP_STATE.GROUNDED) {
			super.updateMotionY(delta);
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		TextureRegion reg = null;
		
		// Apply Skin Color
		batch.setColor(CharacterSkin.values()[GamePreferences.instance.charSkin].getColor());
		
		// Draw image
		reg = regBucket;
		batch.draw(reg.getTexture(), position.x, position.y, origin.x,
				origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation,
				reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(),
				reg.getRegionHeight(), false, false);
		
		// Reset the batch Color to White
		batch.setColor(1, 1, 1, 1);
	}

}