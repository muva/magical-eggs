package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.muvastudios.magiceggfarm.game.Assets;

public class Background extends AbstractGameObject {
	private TextureRegion background;

	public Background() {
		init();
	}

	private void init() {
		dimension.set(10f, 5);
		position.set(-5f, -2.5f);
		background = Assets.instance.levelDecoration.background;
	}

	@Override
	public void render(SpriteBatch batch) {
		TextureRegion reg = null;

		reg = background;
		batch.draw(reg.getTexture(), position.x, position.y, origin.x, origin.y,
				dimension.x, dimension.y, scale.x, scale.y, 0, reg.getRegionX(), reg.getRegionY(),
				reg.getRegionWidth(), reg.getRegionHeight(), false, false);
	}

}
