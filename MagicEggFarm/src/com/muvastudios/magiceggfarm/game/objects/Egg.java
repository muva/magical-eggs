package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.muvastudios.magiceggfarm.game.Assets;

public class Egg extends AbstractGameObject {
	private Array<TextureRegion> regEgg;
	private Array<EggUnit> egg;
	private Bird bird;
	private float eggSpawnTime;

	public class EggUnit extends AbstractGameObject {
		private TextureRegion eggReg;

		public void setRegion(TextureRegion region) {
			eggReg = region;
		}

		@Override
		public void render(SpriteBatch batch) {
			TextureRegion reg = eggReg;
			batch.draw(reg.getTexture(), position.x + 1, position.y + 1,
					origin.x, origin.y, dimension.x, dimension.y, scale.x,
					scale.y, rotation, reg.getRegionX(), reg.getRegionY(),
					reg.getRegionWidth(), reg.getRegionHeight(), false, false);
		}

		@Override
		public void update(float delta) {
			super.update(delta);
			terminalVelocity.set(0, 4.0f);
			velocity.y = -terminalVelocity.y;
		//	position.set(bird.posy.x, bird.posy.y);
		}
	}

	public Egg(Bird bird) {
		bird = new Bird(10, 4);
		this.setBird(bird);
		init();
	}

	private void init() {
		dimension.set(0.3f, 0.3f);
		// Set the bounding box for collision detection
		bounds.set(0, 0, dimension.x, dimension.y);
		regEgg = new Array<TextureRegion>();
		egg = new Array<EggUnit>();
		for (int i = 0; i < 5; i++)
			regEgg.add(Assets.instance.egg.egg);
	}

	private EggUnit spawnEggs() {
		EggUnit eggUnit = new EggUnit();
		eggUnit.dimension.set(dimension);
		eggUnit.setRegion(regEgg.random());
		eggSpawnTime = TimeUtils.nanoTime();
		return eggUnit;

	}

	@Override
	public void render(SpriteBatch batch) {
		if (TimeUtils.nanoTime() - eggSpawnTime > 1000000000) {
			EggUnit eggUnit = spawnEggs();
			egg.add(eggUnit);
		}
		for (EggUnit eggUnit : egg)
			eggUnit.render(batch);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		for (EggUnit eggs : egg)
			eggs.update(delta);
	}

	public int getScore() {
		return 10;
	}

	public Bird getBird() {
		return bird;
	}

	public void setBird(Bird bird) {
		this.bird = bird;
	}

}
