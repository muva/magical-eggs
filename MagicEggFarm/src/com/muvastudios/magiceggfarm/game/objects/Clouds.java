package com.muvastudios.magiceggfarm.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.muvastudios.magiceggfarm.game.Assets;

public class Clouds extends AbstractGameObject {
	private float length;
	private Array<TextureRegion> regClouds;
	private Array<Cloud> clouds;

	private class Cloud extends AbstractGameObject {
		private TextureRegion regCloud;

		public Cloud() {
		}

		public void setRegion(TextureRegion region) {
			regCloud = region;
		}

		@Override
		public void render(SpriteBatch batch) {
			TextureRegion reg = regCloud;
			float relX = -5;
			batch.draw(reg.getTexture(), position.x + origin.x + relX,
					position.y + origin.y, origin.x, origin.y, dimension.x,
					dimension.y, scale.x, scale.y, rotation, reg.getRegionX(),
					reg.getRegionY(), reg.getRegionWidth(),
					reg.getRegionHeight(), false, false);
		}

	}

	public Clouds(float length) {
		this.length = length;
		init();
	}

	private void init() {
		dimension.set(2.0f, 1.2f);
		regClouds = new Array<TextureRegion>();
		regClouds.add(Assets.instance.levelDecoration.cloud01);
		regClouds.add(Assets.instance.levelDecoration.cloud02);
		regClouds.add(Assets.instance.levelDecoration.cloud03);

		int distFac = 2;
		int numClouds = (int) (length / distFac);
		clouds = new Array<Cloud>(2 * numClouds);
		for (int i = 0; i < numClouds; i++) {
			Cloud cloud = spawnCloud();
			cloud.position.x = i * distFac;
			clouds.add(cloud);
		}
	}

	private Cloud spawnCloud() {
		Cloud cloud = new Cloud();
		cloud.dimension.set(dimension);
		// Select random cloud image
		cloud.setRegion(regClouds.random());

		// position
		Vector2 pos = new Vector2();
		pos.x = length - 1; // position after level
		pos.y += 1; // base position

		// random additional position
		pos.y += MathUtils.random(0.0f, 0.2f)
				* (MathUtils.randomBoolean() ? 1 : -1);
		cloud.position.set(pos);
		
		// Speed
		Vector2 speed  = new Vector2();
		speed.x += 0.2f; // base speed
		
		// random additional speed
		speed.x += MathUtils.random(0.0f, 0.55f);
		cloud.terminalVelocity.set(speed);
		
		speed.x *= -1; // Move left
		cloud.velocity.set(speed);
		return cloud;
	}
	
	@Override
	public void update (float delta){
		for (int i = clouds.size -1; i >= 0; i--){
			Cloud cloud = clouds.get(i);
			cloud.update(delta);
			if (cloud.position.x < -2) {
				// cloud moved outside the world.
				// destroy and spawn new cloud at the end of the level
				clouds.removeIndex(i);
				clouds.add(spawnCloud());
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		for (Cloud cloud : clouds)
			cloud.render(batch);
	}

}
