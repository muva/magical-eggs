package com.muvastudios.magiceggfarm.game;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.muvastudios.magiceggfarm.game.objects.Bird.EggUnit;
import com.muvastudios.magiceggfarm.game.objects.Bucket;
import com.muvastudios.magiceggfarm.game.objects.Bucket.JUMP_STATE;
import com.muvastudios.magiceggfarm.game.objects.Ground;
import com.muvastudios.magiceggfarm.game.objects.Ground.GroundUnit;
import com.muvastudios.magiceggfarm.screens.DirectedGame;
import com.muvastudios.magiceggfarm.screens.MenuScreen;
import com.muvastudios.magiceggfarm.transitions.ScreenTransition;
import com.muvastudios.magiceggfarm.transitions.ScreenTransitionSlide;
import com.muvastudios.magiceggfarm.util.AudioManager;
import com.muvastudios.magiceggfarm.util.CameraHelper;
import com.muvastudios.magiceggfarm.util.Constants;

public class WorldController extends InputAdapter {

	// TAG for debugging
	private static final String TAG = WorldController.class.getName();

	// CameraHelper
	public CameraHelper cameraHelper;

	// Level, lives, score and timer
	public Level level;
	public float timeLeft;
	public int score;
	public int target;
	public float scoreVisual;

	// Accelerometer Game Control
	private boolean accelerometerAvailable;

	// Game Over Delay variable
	private float timeLeftGameOverDelay;
	private Rectangle r1 = new Rectangle();
	private Rectangle r2 = new Rectangle();

	// Enable the switch back to menu
	private DirectedGame game;

	public WorldController(DirectedGame game) {
		this.game = game;
		init();
	}

	private void backToMenu() {
		// Screen transition
		ScreenTransition transition = ScreenTransitionSlide.init(0.75f,
				ScreenTransitionSlide.DOWN, false, Interpolation.bounceOut);
		// Switch back to Main Menu
		game.setScreen(new MenuScreen(game), transition);
	}

	private void init() {
		accelerometerAvailable = Gdx.input
				.isPeripheralAvailable(Peripheral.Accelerometer);
		cameraHelper = new CameraHelper();
		timeLeft = Constants.TIME_START;
		timeLeftGameOverDelay = 0;
		initLevel();
	}

	private void initLevel() {
		score = 0;
		scoreVisual = score;
		level = new Level();
		target = 100;
	}

	public void update(float delta) {
		handleDebugInput(delta);
		if (isGameOver()) {
			AudioManager.instance.play(Assets.instance.sound.gameOver);
			timeLeftGameOverDelay -= delta;
			if (timeLeftGameOverDelay < 0) {
				backToMenu();
			}
		} else {
			handleInputGame(delta);
			level.update(delta);
			testCollisions();
			cameraHelper.update(delta);
		}
		timeLeft -= delta;
		scoreTimeController();
		if (!isGameOver()) {
			timeLeftGameOverDelay = Constants.TIME_DELAY_GAME_OVER;
		}
		if (scoreVisual < score) {
			scoreVisual = Math.min(score, scoreVisual + 45 * delta);
		}
		if (scoreVisual > score) {
			scoreVisual = Math.max(score, scoreVisual - 50 * delta);
		}
	}

	// Control score and Time to remain Realistic
	private void scoreTimeController() {
		if (timeLeft < 0) {
			timeLeft = 0;
		} else if (score < 0) {
			score = 0;
		}
	}

	private void testCollisions() {
		// Set First Rectangle to Bucket
		r1.set(level.bucket.position.x, level.bucket.position.y,
				level.bucket.bounds.width, level.bucket.bounds.height);

		// Test Collision: Ground <--> Bucket
		for (GroundUnit unit : level.ground.ground) {
			r2.set(unit.position.x - 5f, unit.position.y - 2.52f,
					unit.bounds.width, unit.bounds.height);
			if (r1.overlaps(r2))
				onCollisionBucketWithGround();
		}

		// Test Collision: Bucket <--> Egg
		for (EggUnit unit : level.bird.normalEggArray) {
			r2.set(unit.position.x - 2.5f, unit.position.y + 0.3f,
					unit.bounds.width, unit.bounds.height);
			if (r1.overlaps(r2)) {
				level.bird.normalEggArray.removeValue(unit, false);
				onCollisionBucketWithEgg();
			}
		}

		// Test Collision: Bucket <--> Bad Egg
		for (EggUnit unit : level.bird.badEggArray) {
			r2.set(unit.position.x - 2.5f, unit.position.y + 0.3f,
					unit.bounds.width, unit.bounds.height);
			if (r1.overlaps(r2)) {
				level.bird.badEggArray.removeValue(unit, false);
				onCollisionBucketWithBadEgg();
			}
		}

		// Test Collision: Bucket <--> Bad Egg
		for (EggUnit unit : level.bird.bonusEggArray) {
			r2.set(unit.position.x - 2.5f, unit.position.y + 0.3f,
					unit.bounds.width, unit.bounds.height);
			if (r1.overlaps(r2)) {
				level.bird.bonusEggArray.removeValue(unit, false);
				onCollisionBucketWithBonusEgg();
			}
		}
	}

	private void onCollisionBucketWithBonusEgg() {
		Gdx.app.debug("Bonus Egg!", "Egg and Bucket");
		AudioManager.instance.play(Assets.instance.sound.bonusEgg);
		score += level.bird.getBonusEggScore();
	}

	private void onCollisionBucketWithBadEgg() {
		Gdx.app.debug("Bad Egg!", "Egg and Bucket");
		AudioManager.instance.play(Assets.instance.sound.badEgg);
		score += level.bird.getBadEggScore();
	}

	private void onCollisionBucketWithEgg() {
		Gdx.app.debug("Normal Egg!", "Egg and Bucket");
		AudioManager.instance.play(Assets.instance.sound.normalEgg);
		score += level.bird.getScore();
	}

	private void onCollisionBucketWithGround() {
		Bucket bucket = level.bucket;
		Ground ground = level.ground;
		switch (bucket.jumpState) {
		case GROUNDED:
			break;
		case FALLING:
		case JUMP_FALLING:
			bucket.position.y = (ground.position.y - 2.52f)
					+ ground.dimension.y;
			bucket.jumpState = JUMP_STATE.GROUNDED;
			break;
		case JUMP_RISING:
			bucket.position.y = (ground.position.y - 2.52f)
					+ ground.dimension.y;
			break;
		}
	}

	// Check whether the game is over
	public boolean isGameOver() {
		return timeLeft == 0;
	}

	private void handleDebugInput(float deltaTime) {
		if (Gdx.app.getType() != ApplicationType.Desktop)
			return;

		// Camera Controls (move)
		float camMoveSpeed = 5 * deltaTime;
		float camMoveSpeedAccelerationFactor = 5;
		if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
			camMoveSpeed *= camMoveSpeedAccelerationFactor;
		if (Gdx.input.isKeyPressed(Keys.LEFT))
			moveCamera(-camMoveSpeed, 0);
		if (Gdx.input.isKeyPressed(Keys.RIGHT))
			moveCamera(camMoveSpeed, 0);
		if (Gdx.input.isKeyPressed(Keys.UP))
			moveCamera(0, camMoveSpeed);
		if (Gdx.input.isKeyPressed(Keys.DOWN))
			moveCamera(0, -camMoveSpeed);
		if (Gdx.input.isKeyPressed(Keys.BACKSPACE))
			cameraHelper.setPosition(0, 0);

		// Camera controls (zoom)
		float camZoomSpeed = 1 * deltaTime;
		float camZoomSpeedAccelerationFactor = 5;
		if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
			camZoomSpeed *= camZoomSpeedAccelerationFactor;
		if (Gdx.input.isKeyPressed(Keys.COMMA))
			cameraHelper.addZoom(camZoomSpeed);
		if (Gdx.input.isKeyPressed(Keys.PERIOD))
			cameraHelper.addZoom(-camZoomSpeed);
		if (Gdx.input.isKeyPressed(Keys.SLASH))
			cameraHelper.setZoom(1);
	}

	private void moveCamera(float x, float y) {
		x += cameraHelper.getPosition().x;
		y += cameraHelper.getPosition().y;
		cameraHelper.setPosition(x, y);
	}

	private void handleInputGame(float delta) {
		// Player Movement
		if (Gdx.input.isKeyPressed(Keys.A)) {
			level.bucket.velocity.x = -level.bucket.terminalVelocity.x;
		} else if (Gdx.input.isKeyPressed(Keys.D)) {
			level.bucket.velocity.x = level.bucket.terminalVelocity.x;
		} else {
			// Use accelerometer for movement if available
			if (accelerometerAvailable){
				// Normalize accelerometer values from [-10, 10] to [-1, 1]
				// Which translate to rotation of [-90, 90] degrees
				float amount = Gdx.input.getAccelerometerY() / 10.0f;
				amount *= 90.0f;
				// is the angle of rotation inside the dead zone?
				if (Math.abs(amount) < Constants.ACCEL_ANGLE_DEAD_ZONE){
					amount = 0;
				} else {
					// use the defined max angle of rotation instead of the 
					// full 90 degrees for maximum velocity
					amount /= Constants.ACCEL_ANGLE_MAX_MOVEMENT;
				}
				level.bucket.velocity.x = level.bucket.terminalVelocity.x * amount;
			}
		}
		// Bucket Jump
		if (Gdx.input.isKeyPressed(Keys.W))
			level.bucket.setJumping(true);
		else
			level.bucket.setJumping(false);
	}

	@Override
	public boolean keyUp(int keycode) {
		// Reset game world
		if (keycode == Keys.R) {
			init();
			Gdx.app.debug(TAG, "Game World Resettled");
		}
		// Back to Main Menu
		else if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {
			backToMenu();
		}
		return false;
	}
}
