package com.muvastudios.magiceggfarm.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.muvastudios.magiceggfarm.util.Constants;

public class Assets implements Disposable, AssetErrorListener {
	public static final String TAG = Assets.class.getName();
	public static final Assets instance = new Assets();
	private AssetManager assetManager;

	// Prevent instantiation from other classes: Singleton
	private Assets() {
	}

	public AssetBucket bucket;
	public AssetEgg egg;
	public AssetGround ground;
	public AssetLevelDecoration levelDecoration;
	public AssetBird bird;
	public AssetTimer timer;
	public AssetFonts fonts;
	public AssetSounds sound;
	public AssetMusic music;

	public void init(AssetManager assetManager) {
		this.assetManager = assetManager;

		// Set asset manager error handler
		assetManager.setErrorListener(this);

		// Load texture atlas
		assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);

		// Load Sounds
		assetManager.load("sound/normal_egg.wav", Sound.class);
		assetManager.load("sound/bad_egg.mp3", Sound.class);
		assetManager.load("sound/bonus_egg.wav", Sound.class);
		assetManager.load("sound/lose_sound.mp3", Sound.class);

		// Load Music
		assetManager.load("music/gameplay_music.mp3", Music.class);
		assetManager.load("music/win_sound.mp3", Music.class);

		// Start loading assets and wait till finished
		assetManager.finishLoading();

		// Debug Message
		Gdx.app.debug(TAG,
				"# of Assets Loaded: " + assetManager.getAssetNames().size);
		for (String a : assetManager.getAssetNames())
			Gdx.app.debug(TAG, "asset: " + a);

		// Get the TextureAtlas
		TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);

		// Ensure All Images are smooth though pixel smoothing
		for (Texture t : atlas.getTextures())
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		// Create the game resource objects
		fonts = new AssetFonts();
		bucket = new AssetBucket(atlas);
		egg = new AssetEgg(atlas);
		ground = new AssetGround(atlas);
		bird = new AssetBird(atlas);
		timer = new AssetTimer(atlas);
		levelDecoration = new AssetLevelDecoration(atlas);
		sound = new AssetSounds(assetManager);
		music = new AssetMusic(assetManager);
	}

	@Override
	public void error(@SuppressWarnings("rawtypes") AssetDescriptor asset,
			Throwable throwable) {
		Gdx.app.error(TAG, "Couldn't load asset '" + asset + "'",
				(Exception) throwable);
	}

	/*
	 * Load assets into logical units. Also cache them right after they are
	 * looked up so that to increase performance
	 */
	/**
	 * Load the bucket
	 * 
	 * @author sam
	 * 
	 */
	public class AssetBucket {
		public final AtlasRegion bucket;

		public AssetBucket(TextureAtlas atlas) {
			bucket = atlas.findRegion("bucket");
		}
	}

	/**
	 * Load the Egg
	 * 
	 * @author sam
	 * 
	 */

	public class AssetEgg {
		public final AtlasRegion egg;
		public final AtlasRegion badEgg;
		public final AtlasRegion bonusEgg;

		public AssetEgg(TextureAtlas atlas) {
			egg = atlas.findRegion("egg");
			badEgg = atlas.findRegion("bad");
			bonusEgg = atlas.findRegion("gold");
		}
	}

	/**
	 * Load the Bird
	 * 
	 */
	public class AssetBird {
		public final Animation birdsAnimation;

		public AssetBird(TextureAtlas atlas) {
			// Animation: Birds
			Array<AtlasRegion> regions = atlas.findRegions("bird");
			birdsAnimation = new Animation(1.0f / 12.0f, regions,
					Animation.LOOP);
		}
	}

	/**
	 * Load the Ground
	 * 
	 * @author sam
	 * 
	 */

	public class AssetGround {
		public final TextureRegion ground;

		public AssetGround(TextureAtlas atlas) {
			ground = atlas.findRegion("ground");
		}
	}

	/**
	 * Load the Timer
	 * 
	 * @author sam
	 * 
	 */
	public class AssetTimer {
		public final TextureRegion timer;

		public AssetTimer(TextureAtlas atlas) {
			timer = atlas.findRegion("clock");
		}

	}

	/**
	 * Load all assets that will be used for level decoration background,
	 * clouds, grasslands and grass
	 * 
	 * @author sam
	 * 
	 */
	public class AssetLevelDecoration {
		public final TextureRegion background;
		public final TextureRegion cloud01;
		public final TextureRegion cloud02;
		public final TextureRegion cloud03;
		public final TextureRegion grassland01;
		public final TextureRegion grassland02;
		public final Animation grassAnimation;

		public AssetLevelDecoration(TextureAtlas atlas) {
			background = atlas.findRegion("gameplay_bg");
			cloud01 = atlas.findRegion("cloud01");
			cloud02 = atlas.findRegion("cloud02");
			cloud03 = atlas.findRegion("cloud03");
			grassland01 = atlas.findRegion("grassland01");
			grassland02 = atlas.findRegion("grassland02");

			// Animation: Grass
			Array<AtlasRegion> regions = atlas.findRegions("grass");
			
			grassAnimation = new Animation(1.0f / 2.0f, regions,
					Animation.LOOP_PINGPONG);
		}
	}

	public class AssetFonts {
		public final BitmapFont defaultSmall;
		public final BitmapFont defaultNormal;
		public final BitmapFont defaultBig;

		public AssetFonts() {
			// Create three fonts: small, medium and big
			defaultSmall = new BitmapFont(
					Gdx.files.internal("images/aubrey.fnt"), true);
			defaultNormal = new BitmapFont(
					Gdx.files.internal("images/aubrey.fnt"), true);
			defaultBig = new BitmapFont(
					Gdx.files.internal("images/aubrey.fnt"), true);

			// Set the font sizes
			defaultSmall.scale(0.25f);
			defaultNormal.scale(1.0f);
			defaultBig.scale(1.5f);

			// Smoothen the Fonts when being drawn
			defaultSmall.getRegion().getTexture()
					.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			defaultNormal.getRegion().getTexture()
					.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			defaultBig.getRegion().getTexture()
					.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}

	}

	public class AssetSounds {
		public final Sound normalEgg;
		public final Sound badEgg;
		public final Sound bonusEgg;
		public final Sound gameOver;

		public AssetSounds(AssetManager am) {
			normalEgg = am.get("sound/normal_egg.wav", Sound.class);
			badEgg = am.get("sound/bad_egg.mp3", Sound.class);
			bonusEgg = am.get("sound/bonus_egg.wav", Sound.class);
			gameOver = am.get("sound/lose_sound.mp3", Sound.class);
		}

	}

	public class AssetMusic {
		public final Music song01;
		public final Music wonLevel;

		public AssetMusic(AssetManager am) {
			song01 = am.get("music/gameplay_music.mp3", Music.class);
			wonLevel = am.get("music/win_sound.mp3", Music.class);
		}

	}

	@Override
	public void dispose() {
		assetManager.dispose();
		fonts.defaultSmall.dispose();
		fonts.defaultNormal.dispose();
		fonts.defaultBig.dispose();
	}

}
