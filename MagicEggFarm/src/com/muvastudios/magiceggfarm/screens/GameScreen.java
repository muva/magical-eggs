package com.muvastudios.magiceggfarm.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.muvastudios.magiceggfarm.game.WorldController;
import com.muvastudios.magiceggfarm.game.WorldRenderer;
import com.muvastudios.magiceggfarm.util.GamePreferences;

public class GameScreen extends AbstractGameScreen {
	// private static final String TAG = GameScreen.class.getName();

	private WorldRenderer worldRenderer;
	private WorldController worldController;

	private boolean paused;

	public GameScreen(DirectedGame game) {
		super(game);
	}

	@Override
	public void render(float delta) {
		// Do not update the game when paused
		if (!paused) {
			// Update the game world by the time that has passed from
			// the last rendered frame
			worldController.update(delta);
		}

		// Sets the clear screen color to: Cornflower Blue
		Gdx.gl.glClearColor(0x64 / 255.0f, 0x95 / 255.0f, 0xed / 255.0f,
				0xff / 255.0f);
		
		// Clears the screen
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		// Render the game world to the screen
		worldRenderer.render();
	}

	@Override
	public void resize(int width, int height) {
		worldRenderer.resize(width, height);
	}

	@Override
	public void show() {
		GamePreferences.instance.load();
		worldController = new WorldController(game);
		worldRenderer = new WorldRenderer(worldController);
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void hide() {
		worldRenderer.dispose();
		Gdx.input.setCatchBackKey(false);
	}

	@Override
	public void pause() {
		paused = true;
	}

	@Override
	public void resume() {
		super.resume();

		// Only called on Android
		paused = false;
	}

	@Override
	public InputProcessor getInputProcessor() {
		return worldController;
	}

}
