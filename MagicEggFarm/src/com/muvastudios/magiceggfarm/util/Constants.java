package com.muvastudios.magiceggfarm.util;

public class Constants {
	// Visible game world is 5m wide
	public static final float VIEWPORT_WIDTH = 5.0f;

	// Visible game world is 5 meters tall
	public static final float VIEWPORT_HEIGHT = 5.0f;

	// GUI Width
	public static final float VIEWPORT_GUI_WIDTH = 800.0f;

	// GUI Height
	public static final float VIEWPORT_GUI_HEIGHT = 480.0f;

	// Location of description file for texture atlas
	public static final String TEXTURE_ATLAS_OBJECTS = "images/magiceggfarm.pack";

	// Location of description file for GUI Atlas
	public static final String TEXTURE_ATLAS_UI = "images/magiceggfarm-ui.pack";

	public static final String TEXTURE_ATLAS_LIBGDX_UI = "images/uiskin.atlas";

	// Location of descriptive for skins
	public static final String SKIN_LIBGDX_UI = "images/uiskin.json";

	public static final String SKIN_MAGICEGGFARM_UI = "images/magiceggfarm-ui.json";

	// Amount of extra lives at level start
	public static final float TIME_START = 60;

	// Delay after the game is over
	public static final int TIME_DELAY_GAME_OVER = 3;
	
	// Preferences 
	public static final String PREFERENCES = "magiceggfarm.prefs";
	
	// Angle of rotation for the Dead Zone (No Movement)
	public static final float ACCEL_ANGLE_DEAD_ZONE = 5.0f;
	
	// Max angle of ration required to gain max speed in Movement
	public static final float ACCEL_ANGLE_MAX_MOVEMENT = 20.0f;
	
}
