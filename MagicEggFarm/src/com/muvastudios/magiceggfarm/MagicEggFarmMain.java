package com.muvastudios.magiceggfarm;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Interpolation;
import com.muvastudios.magiceggfarm.game.Assets;
import com.muvastudios.magiceggfarm.screens.DirectedGame;
import com.muvastudios.magiceggfarm.screens.MenuScreen;
import com.muvastudios.magiceggfarm.transitions.ScreenTransition;
import com.muvastudios.magiceggfarm.transitions.ScreenTransitionSlice;
import com.muvastudios.magiceggfarm.util.AudioManager;
import com.muvastudios.magiceggfarm.util.GamePreferences;

public class MagicEggFarmMain extends DirectedGame {

	public static final String VERSION = "0.8 APHA";

	@Override
	public void create() {
		// Set the log level in the game
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		// Load Assets
		Assets.instance.init(new AssetManager());
		
		// Load preferences for audio settings and start playing music
		GamePreferences.instance.load();
		AudioManager.instance.play(Assets.instance.music.song01);

		// Start the game at Menu Screen
		ScreenTransition transition = ScreenTransitionSlice.init(2,
				ScreenTransitionSlice.UP_DOWN, 10, Interpolation.pow5Out);
		setScreen(new MenuScreen(this), transition);
	}

}
